<?php

//use yii\helpers\Html;
//use yii\widgets\ActiveForm;

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Deal;


/* @var $this yii\web\View */
/* @var $model app\models\Deal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'id')->textInput() ?>

    <!--?= $form->field($model, 'leadid')->textInput() ?-->
	
	<?= $form->field($model, 'leadid')->
				dropDownList(Deal::getLeadid()) ?>

	
	
	

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'amount')->textInput(['maxlength' => true]) ?>
//	
	
	<?php if (\Yii::$app->user->can('updateDeal')) { ?>
	<?= $form->field($model, 'owner')->
				dropDownList(User::getUsers()) ?>    
	<?php } ?>
	
//	

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
